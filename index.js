const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { KnexAdapter: Adapter } = require('@keystonejs/adapter-knex');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const { Text } = require('@keystonejs/fields');
const Todos = require('./lists/Todos.js');
const Tasks = require('./lists/Tasks.js');
const Users = require('./lists/Users.js');

const pg = {
  knexOptions: {
    client: "postgres",
    connection: "postgres://keystone5:k3yst0n3@localhost:5432/keystone"
  }
};

const keystone = new Keystone({
  name: 'keystone',
  adapter: new Adapter(pg),
});

keystone.createList('Todo', Todos);
keystone.createList('Task', Tasks);
keystone.createList('User', Users);

keystone.createItems({
	User: [{ username: 'admin', password: 'adminadmin' }]
});	

const authStrategy = keystone.createAuthStrategy({
	type: PasswordAuthStrategy,
	list: 'User'
});


module.exports = {
  keystone,
  apps: [
	  new GraphQLApp(),
	  new AdminUIApp({
		  enableDefaultRoute: true,
		  authStrategy
	  })
  ],
};

/*
const sqlite = {
	knexOptions: {
		client: 'sqlite3',
		connection: {
			filename: './db.sqlite'
		},
		useNullAsDefault: true
	}
};
*/
